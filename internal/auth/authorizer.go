package auth

import (
	"auth/internal/repo"
	"auth/internal/util/cast"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"github.com/google/uuid"
	"github.com/spf13/viper"
	"golang.org/x/crypto/bcrypt"
	"strconv"
	"time"
)

var (
	ErrInvalidSession = errors.New("invalid session")
)

const (
	UuidLength = 36
)

type Authorizer struct {
	usersTable         *repo.UsersTable
	usersSessionsTable *repo.UsersSessionsTable
	secret             []byte
}

func NewAuthorizer(usersTable *repo.UsersTable, usersSessionsTable *repo.UsersSessionsTable) *Authorizer {
	return &Authorizer{
		usersTable:         usersTable,
		usersSessionsTable: usersSessionsTable,
		secret:             cast.StringToByteArray(viper.GetString("auth.secret")),
	}
}

func (a *Authorizer) CreateUser(user *repo.User) error {
	hashed, err := bcrypt.GenerateFromPassword(cast.StringToByteArray(user.Password), 10)
	if err != nil {
		return err
	}
	user.Password = cast.ByteArrayToString(hashed)
	return a.usersTable.Insert(user)
}

func (a *Authorizer) CreateSession(user repo.User, userAgent, ip, location string) (string, error) {
	id, err := uuid.NewUUID()
	if err != nil {
		return "", err
	}

	idStr := id.String()
	session := repo.UserSession{}
	session.Id = idStr
	session.UserId = user.Id
	session.UserRole = user.Role
	session.UserAgent = userAgent
	session.Ip = ip
	session.Location = location
	now := time.Now()
	session.LastUse = now
	session.CreatedAt = now

	mac := hmac.New(sha256.New, a.getSecret(user.Id))
	mac.Write(cast.StringToByteArray(idStr))
	return idStr + hex.EncodeToString(mac.Sum(nil)), a.usersSessionsTable.Insert(&session)
}

func (a *Authorizer) getSecret(userId int) []byte {
	idStr := strconv.Itoa(userId)
	secret := make([]byte, len(a.secret)+len(idStr))
	secret = append(secret, a.secret...)
	return append(secret, idStr...)
}

func (a *Authorizer) InvalidateSession(session []byte) error {
	return a.usersSessionsTable.DeleteById(cast.ByteArrayToString(session[:UuidLength]))
}

func (a *Authorizer) ValidateSession(session []byte, userAgent, ip, location string) (repo.User, repo.UserSession, error) {
	if len(session) < UuidLength {
		return repo.User{}, repo.UserSession{}, ErrInvalidSession
	}

	sessionMac, err := hex.DecodeString(cast.ByteArrayToString(session[UuidLength:]))
	if err != nil {
		return repo.User{}, repo.UserSession{}, ErrInvalidSession
	}

	var userSession repo.UserSession
	userSession, err = a.usersSessionsTable.GetById(cast.ByteArrayToString(session[:UuidLength]))
	if err != nil {
		return repo.User{}, repo.UserSession{}, err
	}

	mac := hmac.New(sha256.New, a.getSecret(userSession.UserId))
	mac.Write(session[:UuidLength])
	if !hmac.Equal(mac.Sum(nil), sessionMac) {
		return repo.User{}, repo.UserSession{}, ErrInvalidSession
	}

	if userSession.Ip != ip || userSession.Location != location || userSession.UserAgent != userAgent {
		userSession.Ip = ip
		userSession.Location = location
		userSession.UserAgent = userAgent
		err = a.usersSessionsTable.Update(userSession)
		if err != nil {
			return repo.User{}, repo.UserSession{}, err
		}
	}

	var user repo.User
	user, err = a.usersTable.GetById(userSession.UserId)
	return user, userSession, err
}

func (a *Authorizer) ValidateUser(username, password string) (repo.User, error) {
	user, err := a.usersTable.GetByUsername(username)
	if err != nil {
		return repo.User{}, err
	}

	return user, bcrypt.CompareHashAndPassword(cast.StringToByteArray(user.Password), cast.StringToByteArray(password))
}
