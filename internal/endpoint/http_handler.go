package endpoint

import (
	"auth/internal/auth"
	"auth/internal/location"
	"auth/internal/repo"
	"auth/internal/util/cast"
	"encoding/json"
	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"

	"strings"
)

const (
	sessionCookieName  = "session"
	userLocationHeader = "User-Location"
	setCookieHeader    = "Set-Cookie"
)

type HttpHandler struct {
	authorizer       *auth.Authorizer
	locationsService *location.Service
}

func NewHttpHandler(authorizer *auth.Authorizer, locationsService *location.Service) *HttpHandler {
	return &HttpHandler{
		authorizer:       authorizer,
		locationsService: locationsService,
	}
}

func (h *HttpHandler) Handle(ctx *fasthttp.RequestCtx) {
	defer func() {
		err := recover()
		if err != nil {
			logrus.Error(err)
			ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		}
	}()

	switch cast.ByteArrayToString(ctx.Path()) {
	case "/api/v1/login":
		if cast.ByteArrayToString(ctx.Method()) == fasthttp.MethodPost {
			h.login(ctx)
		} else {
			ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		}

	case "/api/v1/signup":
		if cast.ByteArrayToString(ctx.Method()) == fasthttp.MethodPost {
			h.signup(ctx)
		} else {
			ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		}

	case "/api/v1/logout":
		if cast.ByteArrayToString(ctx.Method()) == fasthttp.MethodPost {
			h.logout(ctx)
		} else {
			ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		}

	case "/api/v1/countries":
		if cast.ByteArrayToString(ctx.Method()) == fasthttp.MethodGet {
			h.countries(ctx)
		} else {
			ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		}

	case "/api/v1/locations":
		if cast.ByteArrayToString(ctx.Method()) == fasthttp.MethodGet {
			h.locations(ctx)
		} else {
			ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		}
	}
}

func (h *HttpHandler) locations(ctx *fasthttp.RequestCtx) {
	countryId, err := ctx.QueryArgs().GetUint("country_id")
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	var locations []repo.Location
	locations, err = h.locationsService.GetLocationsById(countryId)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	writeObject(ctx, locations, fasthttp.StatusOK)
}

func (h *HttpHandler) countries(ctx *fasthttp.RequestCtx) {
	writeObject(ctx, h.locationsService.GetAllCountries(), fasthttp.StatusOK)
}

func (h *HttpHandler) authenticate(ctx *fasthttp.RequestCtx) (repo.User, repo.UserSession, bool) {
	session := ctx.Request.Header.Cookie(sessionCookieName)

	userAgent := ctx.UserAgent()
	ip := ctx.RemoteIP().String()
	location := ctx.Request.Header.Peek(userLocationHeader)
	if len(userAgent) == 0 || len(location) == 0 || len(session) == 0 {
		writeError(ctx, "no required data", fasthttp.StatusBadRequest)
		return repo.User{}, repo.UserSession{}, false
	}

	user, userSession, err := h.authorizer.ValidateSession(session, cast.ByteArrayToString(userAgent), ip, cast.ByteArrayToString(location))
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusForbidden)
		return repo.User{}, repo.UserSession{}, false
	}
	return user, userSession, true
}

func (h *HttpHandler) logout(ctx *fasthttp.RequestCtx) {
	err := h.authorizer.InvalidateSession(ctx.Request.Header.Cookie(sessionCookieName))
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}
	ctx.SetStatusCode(fasthttp.StatusNoContent)
}

type loginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (h *HttpHandler) login(ctx *fasthttp.RequestCtx) {
	userAgent := ctx.UserAgent()
	ip := ctx.RemoteIP().String()
	location := ctx.Request.Header.Peek(userLocationHeader)
	if len(userAgent) == 0 || len(location) == 0 {
		writeError(ctx, "no required data", fasthttp.StatusBadRequest)
		return
	}

	req := loginRequest{}
	err := json.Unmarshal(ctx.PostBody(), &req)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	if req.Username == "" || req.Password == "" {
		writeError(ctx, "username or password was not provided", fasthttp.StatusBadRequest)
		return
	}

	var user repo.User
	user, err = h.authorizer.ValidateUser(req.Username, req.Password)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusUnauthorized)
		return
	}

	var session string
	session, err = h.authorizer.CreateSession(user, cast.ByteArrayToString(userAgent), ip, cast.ByteArrayToString(location))
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	ctx.Response.Header.Add(setCookieHeader, h.prepareSessionCookie(session))
	ctx.SetStatusCode(fasthttp.StatusNoContent)
}

func (h *HttpHandler) prepareSessionCookie(session string) string {
	builder := strings.Builder{}
	builder.Grow(len(sessionCookieName) + len(session) + 11)
	builder.WriteString(sessionCookieName)
	builder.WriteByte('=')
	builder.WriteString(session)
	builder.WriteString("; HttpOnly; Secure; SameSite=None; Path=/")
	return builder.String()
}

type signupRequest struct {
	Username    string `json:"username"`
	Password    string `json:"password"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	PhoneNumber int64  `json:"phone_number"`
	LocationId  int    `json:"location_id"`
}

func (h *HttpHandler) signup(ctx *fasthttp.RequestCtx) {
	req := signupRequest{}
	err := json.Unmarshal(ctx.PostBody(), &req)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	if req.Username == "" || req.Password == "" || req.FirstName == "" ||
		req.LastName == "" || req.LocationId <= 0 ||
		req.PhoneNumber < 999_999_999 || req.PhoneNumber > 9_999_999_999 {
		writeError(ctx, "wrong user data", fasthttp.StatusBadRequest)
		return
	}

	user := repo.User{}
	user.Username = req.Username
	user.Password = req.Password
	user.FirstName = req.FirstName
	user.LastName = req.LastName
	user.PhoneNumber = req.PhoneNumber
	user.LocationId = req.LocationId
	user.Status = repo.UserStatusActive

	err = h.authorizer.CreateUser(&user)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusNoContent)
}

func writeObject(ctx *fasthttp.RequestCtx, obj any, status int) {
	row, err := json.Marshal(obj)
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetStatusCode(status)
	ctx.Response.Header.Set(fasthttp.HeaderContentType, "application/json")
	_, _ = ctx.Write(row)
}

type errorResponse struct {
	Error string `json:"error"`
}

func writeError(ctx *fasthttp.RequestCtx, message string, status int) {
	response := errorResponse{Error: message}
	row, err := json.Marshal(&response)
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetStatusCode(status)
	ctx.Response.Header.Set(fasthttp.HeaderContentType, "application/json")
	_, _ = ctx.Write(row)
}

func addCorsHeaders(ctx *fasthttp.RequestCtx) {
	ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
	ctx.Response.Header.Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH")
	ctx.Response.Header.Set("Access-Control-Allow-Headers", "*")
}
