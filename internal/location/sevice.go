package location

import (
	"auth/internal/repo"
	"errors"
)

var ErrCountryNotFound = errors.New("there is no country with such id")

type Service struct {
	countriesTable *repo.CountriesTable
	locationsTable *repo.LocationsTable
}

func NewService(countriesTable *repo.CountriesTable, locationsTable *repo.LocationsTable) *Service {
	return &Service{
		countriesTable: countriesTable,
		locationsTable: locationsTable,
	}
}

func (s *Service) DeleteCountryById(id int) error {
	ok := s.countriesTable.DeleteById(id)
	if !ok {
		return ErrCountryNotFound
	}

	return s.locationsTable.DeleteByCountryId(id)
}

func (s *Service) InsertLocation(location repo.Location) error {
	return s.locationsTable.Insert(&location)
}

func (s *Service) GetAllCountries() []repo.Country {
	return s.countriesTable.GetAll()
}

func (s *Service) GetLocationsById(countryId int) ([]repo.Location, error) {
	return s.locationsTable.GetByCountryId(countryId)
}
