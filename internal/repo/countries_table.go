package repo

import (
	"github.com/spf13/viper"
)

type Country struct {
	Id        int
	Name      string
	PhoneCode int16
}

type CountriesTable struct {
	InMemoryTable[Country]
}

func NewCountriesTable() *CountriesTable {
	t := &CountriesTable{}

	countries := viper.Get("repo.countries").([]interface{})

	idCounter := 0
	for _, c := range countries {
		cVal, ok := c.(map[string]interface{})
		if ok {
			t.Insert(Country{
				Id:        idCounter,
				Name:      cVal["name"].(string),
				PhoneCode: int16(cVal["phone_code"].(int)),
			})
			idCounter++
		}
	}

	return t
}
