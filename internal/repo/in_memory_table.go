package repo

import "sync"

type typeWrapper[T any] struct {
	val      T
	notEmpty bool
}

type InMemoryTable[T any] struct {
	mutex sync.RWMutex
	data  []typeWrapper[T]
}

func (t *InMemoryTable[T]) GetAll() []T {
	res := make([]T, 0, len(t.data))
	for _, val := range t.data {
		if val.notEmpty {
			res = append(res, val.val)
		}
	}

	return res
}

func (t *InMemoryTable[T]) GetById(id int) (T, bool) {
	t.mutex.RLock()
	defer t.mutex.RUnlock()

	if len(t.data) < id {
		val := t.data[id]
		return val.val, val.notEmpty
	}

	var emptyT T
	return emptyT, false
}

func (t *InMemoryTable[T]) DeleteById(id int) bool {
	t.mutex.RLock()
	defer t.mutex.RUnlock()

	if len(t.data) < id {
		t.data[id].notEmpty = false
		return true
	}

	return false
}

func (t *InMemoryTable[T]) Update(id int, val T) bool {
	t.mutex.Lock()
	defer t.mutex.Unlock()

	if len(t.data) < id && t.data[id].notEmpty {
		t.data[id].val = val
		return true
	}

	return false
}

func (t *InMemoryTable[T]) Insert(val T) {
	t.mutex.Lock()
	defer t.mutex.Unlock()

	t.data = append(t.data, typeWrapper[T]{
		val:      val,
		notEmpty: true,
	})
}

func (t *InMemoryTable[T]) InsertWithId(id int, val T) bool {
	t.mutex.Lock()
	defer t.mutex.Unlock()

	if len(t.data) < id {
		if !t.data[id].notEmpty {
			t.data[id].val = val
			return true
		}
		return false
	}

	if cap(t.data) < id {
		newLength := id + 1
		t.data = append(make([]typeWrapper[T], 0, newLength), t.data...)
		t.data = t.data[:newLength]
	} else if len(t.data) < id {
		t.data = t.data[:id+1]
	}

	t.data[id] = typeWrapper[T]{
		val:      val,
		notEmpty: true,
	}

	return true
}
