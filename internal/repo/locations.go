package repo

import (
	"github.com/jackc/pgx"
)

type Location struct {
	Id        int
	Name      string
	CountryId int
}

type LocationsTable struct {
	db                 *pgx.Conn
	getAllStmt         *pgx.PreparedStatement
	getByCountryIdStmt *pgx.PreparedStatement
}

const (
	getAllLocationsQuery        = `SELECT id, name, country_id FROM locations`
	getLocationByCountryIdQuery = `SELECT id, name, country_id FROM locations WHERE country_id = $1`
	insertLocation              = `INSERT INTO locations (name, country_id) VALUES ($1, $2)`
	deleteLocationByCountryId   = `DELETE FROM locations WHERE country_id = $1`
)

func NewLocationsTable(db *pgx.Conn) (*LocationsTable, error) {
	var (
		err         error
		getAllStmt  *pgx.PreparedStatement
		getByIdStmt *pgx.PreparedStatement
	)

	getAllStmt, err = db.Prepare("getAllLocationsQuery", getAllLocationsQuery)
	if err != nil {
		return nil, err
	}
	getByIdStmt, err = db.Prepare("getLocationByIdQuery", getLocationByCountryIdQuery)
	if err != nil {
		return nil, err
	}

	return &LocationsTable{
		db:                 db,
		getAllStmt:         getAllStmt,
		getByCountryIdStmt: getByIdStmt,
	}, nil
}

func (t *LocationsTable) GetAll() ([]Location, error) {
	rows, err := t.db.Query(t.getAllStmt.Name)
	if err != nil {
		return nil, err
	}

	var result []Location
	for rows.Next() {
		var current Location
		err = rows.Scan(&current.Id, &current.Name, &current.CountryId)
		if err != nil {
			return nil, err
		}
		result = append(result, current)
	}
	rows.Close()

	return result, rows.Err()
}

func (t *LocationsTable) GetByCountryId(countryId int) ([]Location, error) {
	rows, err := t.db.Query(t.getByCountryIdStmt.Name, countryId)
	if err != nil {
		return nil, err
	}

	var result []Location
	for rows.Next() {
		var current Location
		err = rows.Scan(&current.Id, &current.Name, &current.CountryId)
		if err != nil {
			return nil, err
		}
		result = append(result, current)
	}
	rows.Close()

	return result, rows.Err()
}

func (t *LocationsTable) Insert(location *Location) error {
	_, err := t.db.Exec(insertLocation, &location.Name, location.CountryId)
	return err
}

func (t *LocationsTable) DeleteByCountryId(countryId int) error {
	_, err := t.db.Exec(deleteLocationByCountryId, countryId)
	return err
}
