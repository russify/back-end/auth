package repo

import (
	"github.com/jackc/pgx"
)

const (
	UserStatusActive = iota
	UserStatusDisabled
)

const (
	UserRolePlainUser = iota
	UserRoleAdmin
)

type User struct {
	Id          int
	Username    string
	Password    string
	FirstName   string
	LastName    string
	PhoneNumber int64
	LocationId  int
	Status      int16
	Role        int16
}

type UsersTable struct {
	db                *pgx.Conn
	getAllStmt        *pgx.PreparedStatement
	getByIdStmt       *pgx.PreparedStatement
	getByUsernameStmt *pgx.PreparedStatement
	insertStmt        *pgx.PreparedStatement
}

const (
	getAllUsersQuery       = `SELECT id, username, password, first_name, last_name, phone_number, location_id, status, role FROM users`
	getUserByIdQuery       = `SELECT id, username, password, first_name, last_name, phone_number, location_id, status, role FROM users WHERE id = $1`
	getUserByUsernameQuery = `SELECT id, username, password, first_name, last_name, phone_number, location_id, status, role FROM users WHERE username = $1`
	insertUser             = `INSERT INTO users (username, password, first_name, last_name, phone_number, location_id, status, role) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`
)

func NewUsersTable(db *pgx.Conn) (*UsersTable, error) {
	var (
		err               error
		getAllStmt        *pgx.PreparedStatement
		getByIdStmt       *pgx.PreparedStatement
		insertStmt        *pgx.PreparedStatement
		getByUsernameStmt *pgx.PreparedStatement
	)

	getAllStmt, err = db.Prepare("getAllUsersQuery", getAllUsersQuery)
	if err != nil {
		return nil, err
	}
	getByIdStmt, err = db.Prepare("getUserByIdQuery", getUserByIdQuery)
	if err != nil {
		return nil, err
	}
	getByUsernameStmt, err = db.Prepare("getUserByUsernameQuery", getUserByUsernameQuery)
	if err != nil {
		return nil, err
	}
	insertStmt, err = db.Prepare("insertUser", insertUser)
	if err != nil {
		return nil, err
	}

	return &UsersTable{
		db:                db,
		getAllStmt:        getAllStmt,
		getByIdStmt:       getByIdStmt,
		insertStmt:        insertStmt,
		getByUsernameStmt: getByUsernameStmt,
	}, nil
}

func (t *UsersTable) GetAll() ([]User, error) {
	rows, err := t.db.Query(t.getAllStmt.Name)
	if err != nil {
		return nil, err
	}

	var result []User
	for rows.Next() {
		var user User
		err = rows.Scan(&user.Id,
			&user.Username,
			&user.Password,
			&user.FirstName,
			&user.LastName,
			&user.PhoneNumber,
			&user.LocationId,
			&user.Status,
			&user.Role)
		if err != nil {
			return nil, err
		}
		result = append(result, user)
	}
	rows.Close()

	return result, rows.Err()
}

func (t *UsersTable) GetById(id int) (User, error) {
	row := t.db.QueryRow(t.getByIdStmt.Name, id)

	var user User
	err := row.Scan(&user.Id,
		&user.Username,
		&user.Password,
		&user.FirstName,
		&user.LastName,
		&user.PhoneNumber,
		&user.LocationId,
		&user.Status,
		&user.Role)

	return user, err
}

func (t *UsersTable) GetByUsername(username string) (User, error) {
	row := t.db.QueryRow(t.getByUsernameStmt.Name, username)

	var user User
	err := row.Scan(&user.Id,
		&user.Username,
		&user.Password,
		&user.FirstName,
		&user.LastName,
		&user.PhoneNumber,
		&user.LocationId,
		&user.Status,
		&user.Role)

	return user, err
}

func (t *UsersTable) Insert(user *User) error {
	_, err := t.db.Exec(t.insertStmt.Name,
		&user.Username,
		&user.Password,
		&user.FirstName,
		&user.LastName,
		&user.PhoneNumber,
		&user.LocationId,
		&user.Status,
		&user.Role)
	return err
}
