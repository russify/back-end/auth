package repo

import (
	"github.com/jackc/pgx"
	"time"
)

type UserSession struct {
	Id        string
	Ip        string
	Location  string
	UserAgent string
	CreatedAt time.Time
	LastUse   time.Time
	UserId    int
	UserRole  int16
}

type UsersSessionsTable struct {
	db             *pgx.Conn
	getByIdStmt    *pgx.PreparedStatement
	insertStmt     *pgx.PreparedStatement
	updateStmt     *pgx.PreparedStatement
	deleteById     *pgx.PreparedStatement
	deleteByUserId *pgx.PreparedStatement
}

const (
	getUserSessionByIdQuery         = `SELECT id, user_id, ip, location, user_agent, created_at, last_use, role FROM users_sessions WHERE id = $1`
	insertUserSessionQuery          = `INSERT INTO users_sessions (id, user_id, ip, location, user_agent, created_at, last_use, role) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`
	updateUserSessionQuery          = `UPDATE users_sessions SET user_id = $2, ip = $3, location = $4, user_agent = $5, created_at = $6, last_use = $7 WHERE id = $1`
	deleteUserSessionByIdQuery      = `DELETE FROM users_sessions WHERE id = $1`
	deleteUserSessionsByUserIdQuery = `DELETE FROM users_sessions WHERE user_id = $1`
)

func NewUsersSessionsTable(db *pgx.Conn) (*UsersSessionsTable, error) {
	var (
		err                error
		getByIdStmt        *pgx.PreparedStatement
		insertStmt         *pgx.PreparedStatement
		updateStmt         *pgx.PreparedStatement
		deleteByIdStmt     *pgx.PreparedStatement
		deleteByUserIdStmt *pgx.PreparedStatement
	)

	getByIdStmt, err = db.Prepare("getUserSessionByIdQuery", getUserSessionByIdQuery)
	if err != nil {
		return nil, err
	}
	insertStmt, err = db.Prepare("insertUserSessionQuery", insertUserSessionQuery)
	if err != nil {
		return nil, err
	}
	updateStmt, err = db.Prepare("updateUserSessionQuery ", updateUserSessionQuery)
	if err != nil {
		return nil, err
	}
	deleteByIdStmt, err = db.Prepare("deleteUserSessionByIdQuery", deleteUserSessionByIdQuery)
	if err != nil {
		return nil, err
	}
	deleteByUserIdStmt, err = db.Prepare("deleteUserSessionsByUserIdQuery", deleteUserSessionsByUserIdQuery)
	if err != nil {
		return nil, err
	}

	return &UsersSessionsTable{
		db:             db,
		getByIdStmt:    getByIdStmt,
		insertStmt:     insertStmt,
		updateStmt:     updateStmt,
		deleteById:     deleteByIdStmt,
		deleteByUserId: deleteByUserIdStmt,
	}, nil
}

func (t *UsersSessionsTable) GetById(id string) (UserSession, error) {
	row := t.db.QueryRow(t.getByIdStmt.Name, id)

	var session UserSession
	err := row.Scan(&session.Id,
		&session.UserId,
		&session.Ip,
		&session.Location,
		&session.UserAgent,
		&session.CreatedAt,
		&session.LastUse)

	return session, err
}

func (t *UsersSessionsTable) Update(session UserSession) error {
	_, err := t.db.Exec(t.updateStmt.Name,
		&session.Id,
		&session.UserId,
		&session.Ip,
		&session.Location,
		&session.UserAgent,
		&session.CreatedAt,
		&session.LastUse,
	)
	return err
}

func (t *UsersSessionsTable) DeleteById(id string) error {
	_, err := t.db.Exec(t.deleteById.Name, id)
	return err
}

func (t *UsersSessionsTable) DeleteByUserId(userId int) error {
	_, err := t.db.Exec(t.deleteByUserId.Name, userId)
	return err
}

func (t *UsersSessionsTable) Insert(session *UserSession) error {
	_, err := t.db.Exec(t.insertStmt.Name,
		&session.Id,
		&session.UserId,
		&session.Ip,
		&session.Location,
		&session.UserAgent,
		&session.CreatedAt,
		&session.LastUse,
		&session.UserRole,
	)
	return err
}
