package main

import (
	"auth/internal/auth"
	"auth/internal/endpoint"
	"auth/internal/location"
	"auth/internal/logger"
	"auth/internal/repo"
	"github.com/jackc/pgx"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/valyala/fasthttp"
	"log"
	"os"
	"os/signal"
)

var (
	dbConn             *pgx.Conn
	usersTable         *repo.UsersTable
	usersSessionsTable *repo.UsersSessionsTable
	countriesTable     *repo.CountriesTable
	locationsTable     *repo.LocationsTable

	authorizer       *auth.Authorizer
	locationsService *location.Service

	httpHandler *endpoint.HttpHandler
)

func main() {
	logger.Initialize()
	setupConfig()
	setupDatabase()
	setupTables()
	setupAuthorizer()

	locationsService = location.NewService(countriesTable, locationsTable)

	httpHandler = endpoint.NewHttpHandler(authorizer, locationsService)
	go func() {
		logrus.Info("Server was started")
		err := fasthttp.ListenAndServe("0.0.0.0:8001", httpHandler.Handle)
		if err != nil {
			logrus.Warn(err.Error())
		}
	}()

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	<-sigChan
}

func setupConfig() {
	viper.SetConfigName("configuration")
	viper.SetConfigType("yml")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		logrus.Fatal(err.Error())
	}

	// database default values
	viper.SetDefault("sql.hostname", "localhost")
	viper.SetDefault("sql.port", "5432")
	viper.SetDefault("sql.user", "postgres")
}

func setupDatabase() {
	host := viper.GetString("sql.hostname")
	port := viper.GetUint32("sql.port")
	user := viper.GetString("sql.user")
	password := viper.GetString("sql.password")
	databaseName := viper.GetString("sql.database")

	logrus.Info(host)

	var err error
	dbConn, err = pgx.Connect(pgx.ConnConfig{
		Host:         host,
		Port:         uint16(port),
		Database:     databaseName,
		User:         user,
		Password:     password,
		CustomCancel: func(_ *pgx.Conn) error { return nil },
	})
	if err != nil {
		log.Fatalln("Error occurred during opening database connection: ",
			err.Error())
	}
}

func setupTables() {
	var err error
	usersTable, err = repo.NewUsersTable(dbConn)
	if err != nil {
		logrus.Fatal(err.Error())
	}
	usersSessionsTable, err = repo.NewUsersSessionsTable(dbConn)
	if err != nil {
		logrus.Fatal(err.Error())
	}
	locationsTable, err = repo.NewLocationsTable(dbConn)
	if err != nil {
		logrus.Fatal(err.Error())
	}
	countriesTable = repo.NewCountriesTable()
}

func setupAuthorizer() {
	authorizer = auth.NewAuthorizer(usersTable, usersSessionsTable)
}
