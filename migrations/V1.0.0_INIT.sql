CREATE DATABASE russify_auth ENCODING='UTF8';

CREATE TABLE locations
(
    id         SERIAL PRIMARY KEY,
    name       VARCHAR UNIQUE NOT NULL,
    country_id INTEGER        NOT NULL
);

CREATE TABLE users
(
    id           SERIAL PRIMARY KEY,
    username     VARCHAR UNIQUE NOT NULL,
    password     VARCHAR        NOT NULL,
    first_name   VARCHAR        NOT NULL,
    last_name    VARCHAR        NOT NULL,
    phone_number BIGINT         NOT NULL,
    location_id  INTEGER REFERENCES locations (id) ON DELETE SET NULL ON UPDATE CASCADE,
    status       SMALLINT       NOT NULL DEFAULT 0,
    role         SMALLINT       NOT NULL DEFAULT 0
);

CREATE TABLE users_sessions
(
    id         VARCHAR(36) PRIMARY KEY,
    role       SMALLINT                                                          NOT NULL,
    user_id    INTEGER REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE NOT NULL,
    ip         VARCHAR                                                           NOT NULL,
    location   VARCHAR                                                           NOT NULL,
    user_agent VARCHAR                                                           NOT NULL,
    created_at TIMESTAMP                                                         NOT NULL,
    last_use   TIMESTAMP                                                         NOT NULL
);
